package com.univnantes.iotapplication;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.univnantes.iotapplication.dao.URLManager;
import com.univnantes.iotapplication.dao.Urls;

import org.w3c.dom.Text;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Ajout_Activity extends AppCompatActivity {

    private boolean MenuOvert = false;


    private Button Menuroom;
    private Button MenuAjout;
    private Button MenuEnvoyer;

    private DrawerLayout drawerLayout;
    private Button ajouterUrl;
    private EditText nouvelleUrl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout);


        drawerLayout = (DrawerLayout) findViewById(R.id.menu_layout);

        MenuAjout = (Button) findViewById(R.id.bouttonMenuAjouter);
        Menuroom = (Button) findViewById(R.id.bouttonMenuroom);
        ajouterUrl = (Button) findViewById(R.id.ajout);
        nouvelleUrl = (EditText) findViewById(R.id.urlText);
        MenuEnvoyer = (Button) findViewById(R.id.bouttonMenuEnvoyer);

        MenuEnvoyer.setOnClickListener(envoyerClique);
        MenuAjout.setOnClickListener(ajoutClique);
        Menuroom.setOnClickListener(roomClique);
        ajouterUrl.setOnClickListener(ajouterUrlClique);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.hamburgerMenu:
                if (MenuOvert) {
                    drawerLayout.closeDrawer(Gravity.RIGHT);
                    MenuOvert = false;
                } else {
                    drawerLayout.openDrawer(Gravity.RIGHT);
                    MenuOvert = true;
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }


    private View.OnClickListener roomClique = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Intent i = new Intent(Ajout_Activity.this, MainActivity.class);
            Ajout_Activity.this.startActivity(i);
        }
    };


    private View.OnClickListener ajoutClique = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Intent i = new Intent(Ajout_Activity.this, Ajout_Activity.class);
            Ajout_Activity.this.startActivity(i);
        }
    };

    private View.OnClickListener ajouterUrlClique = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (nouvelleUrl.getText() != null && !nouvelleUrl.getText().toString().equals("url")) {
                URLManager m = new URLManager(Ajout_Activity.this);
                m.open();
                m.addURL(new Urls(0, nouvelleUrl.getText().toString()));
                m.close();
                Intent i = new Intent(Ajout_Activity.this, MainActivity.class);
                Ajout_Activity.this.startActivity(i);
            } else {
                Toast.makeText(Ajout_Activity.this, "vous n'avez pas renseigné d'url", Toast.LENGTH_LONG).show();
            }
        }
    };
    /**
     * traitement du click sur le bouton "envoyer"
     */
    private View.OnClickListener envoyerClique = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Intent i = new Intent(Ajout_Activity.this, Envoyer_Activity.class);
            Ajout_Activity.this.startActivity(i);
        }
    };
}