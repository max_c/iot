package com.univnantes.iotapplication.dao;

public class Urls {
    private long id;
    private String url;

    public Urls(long id, String url) {
        super();
        this.id = id;
        this.url = url;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


}