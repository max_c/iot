package com.univnantes.iotapplication.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class URLManager {


    private static final String TABLE_NAME = "url";
    public static final String KEY_ID_URL="id_url";
    public static final String KEY_NOM_URL="url";
    public static final String CREATE_TABLE_URL = "CREATE TABLE "+TABLE_NAME+
            " (" +
            " "+KEY_ID_URL+" INTEGER primary key," +
            " "+KEY_NOM_URL+" TEXT" +
            ");";
    private MySQLite maBaseSQLite; // notre gestionnaire du fichier SQLite
    private SQLiteDatabase db;

    public URLManager(Context context)
    {
        maBaseSQLite = MySQLite.getInstance(context);
    }

    public void open()
    {
        db = maBaseSQLite.getWritableDatabase();
    }

    public void close()
    {
        db.close();
    }

    public long addURL(Urls urls) {

        ContentValues values = new ContentValues();
        values.put(KEY_NOM_URL, urls.getUrl());
        return db.insert(TABLE_NAME,null,values);
    }

    public int modURL(Urls url) {

        ContentValues values = new ContentValues();
        values.put(KEY_NOM_URL, url.getUrl());

        String where = KEY_ID_URL+" = ?";
        String[] whereArgs = {url.getId()+""};

        return db.update(TABLE_NAME, values, where, whereArgs);
    }

    public int supURL(Urls url) {

        String where = KEY_ID_URL+" = ?";
        String[] whereArgs = {url.getId()+""};

        return db.delete(TABLE_NAME, where, whereArgs);
    }

    public Urls getURL(int id) {

        Urls a=new Urls(0,"");

        Cursor c = db.rawQuery("SELECT * FROM "+TABLE_NAME+" WHERE "+KEY_ID_URL+"="+id, null);
        if (c.moveToFirst()) {
            a.setId(c.getInt(c.getColumnIndex(KEY_ID_URL)));
            a.setUrl(c.getString(c.getColumnIndex(KEY_NOM_URL)));
            c.close();
        }

        return a;
    }

    public Cursor getUrls() {
        return db.rawQuery("SELECT * FROM "+TABLE_NAME, null);
    }
}
