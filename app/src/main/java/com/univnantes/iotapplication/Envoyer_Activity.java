package com.univnantes.iotapplication;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.univnantes.iotapplication.dao.URLManager;
import com.univnantes.iotapplication.dao.Urls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Envoyer_Activity extends AppCompatActivity {

    private boolean MenuOvert = false;


    private Button Menuroom;
    private Button MenuAjout;
    private Button MenuEnvoyer;

    private DrawerLayout drawerLayout;
    private Button envoyer;
    private EditText nouvelleUrl;
    private EditText messageText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_envoyer);


        drawerLayout = (DrawerLayout) findViewById(R.id.menu_layout);

        MenuAjout = (Button) findViewById(R.id.bouttonMenuAjouter);
        Menuroom = (Button) findViewById(R.id.bouttonMenuroom);
        envoyer = (Button) findViewById(R.id.envoyer);
        nouvelleUrl = (EditText) findViewById(R.id.urlTextActivityEnvoyee);
        messageText = (EditText) findViewById(R.id.messageAEnvoyer);

        MenuEnvoyer = (Button) findViewById(R.id.bouttonMenuEnvoyer);

        MenuEnvoyer.setOnClickListener(envoyerClique);
        MenuAjout.setOnClickListener(ajoutClique);
        Menuroom.setOnClickListener(roomClique);
        envoyer.setOnClickListener(envoyerMessageClique);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.hamburgerMenu:
                if (MenuOvert) {
                    drawerLayout.closeDrawer(Gravity.RIGHT);
                    MenuOvert = false;
                } else {
                    drawerLayout.openDrawer(Gravity.RIGHT);
                    MenuOvert = true;
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }


    private View.OnClickListener roomClique = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Intent i = new Intent(Envoyer_Activity.this, MainActivity.class);
            Envoyer_Activity.this.startActivity(i);
        }
    };


    private View.OnClickListener ajoutClique = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Intent i = new Intent(Envoyer_Activity.this, Ajout_Activity.class);
            Envoyer_Activity.this.startActivity(i);
        }
    };

    private View.OnClickListener envoyerMessageClique = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (nouvelleUrl.getText() != null && !nouvelleUrl.getText().toString().equals("url")) {
                if(messageText.getText() != null && !messageText.getText().toString().equals("votre message")){
                    try {
                        postRequest(nouvelleUrl.getText().toString(), messageText.getText().toString());
                        Log.w("mmmmmm","message envoyer");
                    } catch (IOException e) {
                        Log.w("mmmmmm","message non envoyer");

                        e.printStackTrace();
                    }
                    Toast.makeText(Envoyer_Activity.this, "Message envoyé", Toast.LENGTH_LONG).show();
                    Intent i = new Intent(Envoyer_Activity.this, MainActivity.class);
                    Envoyer_Activity.this.startActivity(i);
                }else{
                    Toast.makeText(Envoyer_Activity.this, "vous n'avez pas renseigner de message", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(Envoyer_Activity.this, "vous n'avez pas renseigné d'url", Toast.LENGTH_LONG).show();
            }
        }
    };
    /**
     * traitement du click sur le bouton "envoyer"
     */
    private View.OnClickListener envoyerClique = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Intent i = new Intent(Envoyer_Activity.this, Envoyer_Activity.class);
            Envoyer_Activity.this.startActivity(i);
        }
    };


    public void postRequest(String url, String message) throws IOException {

        MediaType MEDIA_TYPE = MediaType.parse("application/json");
        OkHttpClient client = new OkHttpClient();

        JSONObject postdata = new JSONObject();
        try {
            postdata.put("write_api_key","9UIX7O55WNIO6NMT");
            Date   now = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/YYYY HH:mm");
            String date = formatter.format(now);

            postdata.put("updates",new JSONArray().put(new JSONObject().put("created_at", date).put("field1", message).put("field2", message)));
        } catch(JSONException e){
            e.printStackTrace();
        }
        Log.w("message",postdata.toString());
        RequestBody body = RequestBody.create(MEDIA_TYPE, postdata.toString());

        Request request = new Request.Builder()
                .url("https://api.thingspeak.com/channels/761853/bulk_update.json")
                .post(body)
                .header("Host", "api.thingspeak.com")
                .header("Content-Type", "application/json")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                String mMessage = e.getMessage().toString();
                Log.w("failure Response", mMessage);
                //call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.w("REUSSSI", response.body().string());

            }
        });
    }
}
