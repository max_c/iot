package com.univnantes.iotapplication;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.univnantes.iotapplication.dao.URLManager;
import com.univnantes.iotapplication.dao.Urls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class MainActivity extends AppCompatActivity implements Callback {

    private LinearLayout linearLayout;
    private LinearLayout listeOfRooms;
    private final OkHttpClient client = new OkHttpClient();
    private boolean MenuOvert = false;
    private DrawerLayout drawerLayout;
    private Button refresh;
    private Button Menuroom;
    private Button MenuAjout;
    private Button MenuEnvoyer;

    ArrayList<String> listOfUrlPiece;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        listOfUrlPiece = initListPiece();//https://api.thingspeak.com/channels/761181/fields/1.json?api_key=B0W0NT8G2P8HLRLY&results=2

        drawerLayout = (DrawerLayout) findViewById(R.id.menu_layout);
        MenuAjout = (Button) findViewById(R.id.bouttonMenuAjouter);
        Menuroom = (Button) findViewById(R.id.bouttonMenuroom);
        MenuEnvoyer = (Button) findViewById(R.id.bouttonMenuEnvoyer);
        linearLayout = (LinearLayout) findViewById(R.id.linearMain);
        listeOfRooms = (LinearLayout) findViewById(R.id.listeRooms);
        refresh = (Button) findViewById(R.id.refresh);
        MenuAjout.setOnClickListener(ajoutClique);
        Menuroom.setOnClickListener(roomClique);
        MenuEnvoyer.setOnClickListener(envoyerClique);
        this.getRoomsInformations();

    }

    /**
     * recupere la liste des pieces presente dans la base de données
     *
     * @return
     */
    private ArrayList<String> initListPiece() {
        ArrayList<String> tmp = new ArrayList<>();
        URLManager m = new URLManager(this);
        m.open();
        Cursor c = m.getUrls();
        if (c.moveToFirst()) {
            do {
                Log.d("url -> ",
                        c.getInt(c.getColumnIndex(URLManager.KEY_ID_URL)) + "," +
                                c.getString(c.getColumnIndex(URLManager.KEY_NOM_URL))
                );
                tmp.add(c.getString(c.getColumnIndex(URLManager.KEY_NOM_URL)));
            }
            while (c.moveToNext());
        }
        c.close();
        m.close();
        return tmp;
    }

    /**
     * gestion du menu
     *
     * @param item
     * @return
     */
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.hamburgerMenu:
                if (MenuOvert) {
                    drawerLayout.closeDrawer(Gravity.RIGHT);
                    MenuOvert = false;
                } else {
                    drawerLayout.openDrawer(Gravity.RIGHT);
                    MenuOvert = true;
                }

                return true;
            case R.id.refresh:
                listeOfRooms.removeAllViews();
                MainActivity.this.getRoomsInformations();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * gestion du menu
     *
     * @param menu
     * @return
     */
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    /**
     * traitement du click sur le bouton "rooms"
     */
    private View.OnClickListener roomClique = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Intent i = new Intent(MainActivity.this, MainActivity.class);

            MainActivity.this.startActivity(i);
        }
    };

    /**
     * traitement du click sur le bouton "envoyer"
     */
    private View.OnClickListener envoyerClique = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Intent i = new Intent(MainActivity.this, Envoyer_Activity.class);

            MainActivity.this.startActivity(i);
        }
    };

    /**
     * traitement du click sur le bouton "ajout"
     */
    private View.OnClickListener ajoutClique = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Intent i = new Intent(MainActivity.this, Ajout_Activity.class);
            startActivity(i);
        }
    };

    /**
     * recupere les infos sur les pieces presentes où les urls de la base de données pointes
     */
    private void getRoomsInformations() {
        if (!isConnected()) {
            Toast.makeText(MainActivity.this, "probleme de connexion", Toast.LENGTH_LONG).show();
            return;
        }

        if (listOfUrlPiece == null || listOfUrlPiece.size() < 1) {
            Toast.makeText(MainActivity.this, "aucune url connue", Toast.LENGTH_LONG).show();
            return;
        }
        Toast.makeText(MainActivity.this, "chargement des données", Toast.LENGTH_LONG).show();
        //Récupération des informations de chaque pièce
        for (String url : listOfUrlPiece) {
            Request request = new Request.Builder().url(url).build();
            client.newCall(request).enqueue(MainActivity.this);
        }
    }

    /**
     * vérifie si la connection est établie
     *
     * @return
     */
    private boolean isConnected() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    @Override
    public void onFailure(Call call, IOException e) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showInformationInString("ERREUR", "Erreur lors de l'exécution de la requête");
            }
        });
    }

    /**
     * traitement des reponse des requetes http envoyées
     *
     * @param call
     * @param response
     * @throws IOException
     */
    @Override
    public void onResponse(Call call, Response response) throws IOException {
        if (!response.isSuccessful()) {
            throw new IOException(response.toString());
        }

        try {
            final JSONObject jsonObject = new JSONObject(response.body().string());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        JSONArray array = jsonObject.getJSONArray("feeds");

                        assert array != null;
                        if (array.length() > 0) {
                            boolean findLastInfo = false;
                            for (int i = 0; i < array.length(); ++i) {
                                if (array.getJSONObject(i).getInt("entry_id") == jsonObject.getJSONObject("channel").getInt("last_entry_id")) {
                                    showInformation(jsonObject.getJSONObject("channel").getString("name"), array.getJSONObject(i));
                                    findLastInfo = true;
                                    break;
                                }
                            }
                            if (!findLastInfo) {
                                showInformationInString(jsonObject.getJSONObject("channel").getString("name"), "aucune température a été relevée pour le moment");
                            }

                        } else {
                            showInformationInString(jsonObject.getJSONObject("channel").getString("name"), "aucune température a été relevée pour le moment");
                        }
                    } catch (JSONException e) {
                        showInformationInString("ERREUR", "Erreur survenue lors du traitement de la réponse");
                        e.printStackTrace();
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * affichage des informations issus des réponses aux requetes http
     *
     * @param piece
     * @param info
     */
    private void showInformationInString(String piece, String info) {
        TextView nomPiece = new TextView(this);
        nomPiece.setText(piece);
        nomPiece.setGravity(0);

        TextView temperature = new TextView(this);
        temperature.setText(info);

        LinearLayout pieceLayout = new LinearLayout(this);
        pieceLayout.setOrientation(LinearLayout.VERTICAL);
        pieceLayout.addView(nomPiece);
        pieceLayout.addView(temperature);

        listeOfRooms.addView(pieceLayout);
    }

    /**
     * affichage des informations issus des réponses aux requetes http
     *
     * @param piece
     * @param info
     */
    private void showInformation(String piece, JSONObject info) throws JSONException {
        LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout child = (LinearLayout) inflater.inflate(R.layout.roomlayout, null);
        ((TextView) child.findViewById(R.id.titleRoom)).setText(piece);
        ((TextView) child.findViewById(R.id.temperature)).setText(info.getString("field1")+" °C");
        ((TextView) child.findViewById(R.id.humidity)).setText(info.getString("field2")+" %");

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(15, 5, 15, 0);
        child.setLayoutParams(params);

        listeOfRooms.addView(child);
    }
}