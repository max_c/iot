#include <WiFi.h>
#include <string.h>
#include "ThingSpeak.h"
#include <Wire.h> // driver I2C
#include "SparkFunHTU21D.h"
#include <U8x8lib.h> // bibliothèque à charger a partir de

U8X8_SSD1306_128X64_NONAME_SW_I2C u8x8(15,4,16); // clock, data, reset
HTU21D myTempHumi;

char ssid[] = "********"; // your network SSID (name)
char pass[] = "*********"; // your network passw
unsigned long myChannelNumber = 761181;
unsigned long myChannelNumber2 = 761853;  
const char * myWriteAPIKey="N1909NHAJRYL7E30" ;
const char * myReadAPIKey ="IKPV7OGHVZJWNT9Z" ;
WiFiClient client;

void setup() {
  Serial.println("Start");
  Serial.begin(9600);
  myTempHumi.begin();
  Serial.println("Erasing previous WIFI");
  WiFi.disconnect(true); // effacer de l’EEPROM WiFi credentials
  
  delay(1000);
  Serial.println("Connecting to wifi");
  WiFi.begin(ssid, pass);
  delay(1000);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
  Serial.println("WiFi setup ok");
  delay(1000);
  ThingSpeak.begin(client); // connexion (TCP) du client au serveur
  delay(1000);
  Serial.println("ThingSpeak begin");
  
  Serial.begin(9600);
  u8x8.begin(); // initialize OLED
  u8x8.setFont(u8x8_font_chroma48medium8_r);
  Serial.begin(1000);

  Serial.begin(9600);
  myTempHumi.begin();
  delay(1000); 
}

int tout=10000; // en millisecondes
float temp,humi;

void loop()
{
  humi = myTempHumi.readHumidity();
  temp = myTempHumi.readTemperature();

  ThingSpeak.setField(2, humi); // préparation du field1
  ThingSpeak.setField(1, temp); // préparation du field2
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    dispData1();
  }  
  ThingSpeak.writeFields(myChannelNumber, myWriteAPIKey);
  dispData2();
  dispData3();
}


void dispData1()
{
  char dbuf[16];
  u8x8.clear();
  Serial.println("Connexion au wifi en cours");
  u8x8.drawString(0,1,"Connexion "); // 0 – colonne (max 15), 1 – ligne (max 7)
  u8x8.drawString(0,2,"au wifi");
  u8x8.drawString(0,3,"en cours");
  delay(6000);
}

void dispData2()
{
  char dbuf[32];
  u8x8.clear();
  Serial.println("Connecte au wifi");
  u8x8.drawString(0,1,"Connection wifi"); // 0 – colonne (max 15), 1 – ligne (max 7)
  sprintf(dbuf,"Temperat:%.2f C",temp); u8x8.drawString(0,3,dbuf);
  sprintf(dbuf,"Humidity:%.2f %",humi); u8x8.drawString(0,4,dbuf);

  Serial.print(" Temperature:");
  Serial.print(temp, 1);
  Serial.print("C");
  Serial.print(" Humidity:");
  Serial.print(humi, 1);
  Serial.print("%");
  Serial.println();  
  delay(tout);
}

void dispData3()
{
  char rec[15];
  String temp = ThingSpeak.readStringField(myChannelNumber2,1,myReadAPIKey);
  temp.toCharArray(rec,temp.length()+1);
  u8x8.clear();
  Serial.println("Recoit");
  Serial.print("Status: ");
  Serial.println(ThingSpeak.getLastReadStatus());
  Serial.println(temp);
  u8x8.drawString(0,1,"Recoit");
  u8x8.drawString(0,3,rec);
  delay(tout);
}
